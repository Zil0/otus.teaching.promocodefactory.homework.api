﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;


namespace Otus.Teaching.PromoCodeFactory.SignalRListener
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/customerHub")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();


            connection.On("ReceiveCustomerUpdate", (string count) =>
            {
                // show alert to the user
                Console.WriteLine($"Customers: {count}");
            });

            await connection.InvokeAsync<string>("SubscribeOnCustomerUpdate", "ALL");

            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/customerHub")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();


            //GetCustomersAsync
            var result = await connection.InvokeAsync<IEnumerable<CustomerShortResponse>>("GetCustomersAsync");
            Console.WriteLine("GetCustomersAsync Done");

            //GetCustomerAsync
            var id = new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
            var result2 = await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync", id);
            Console.WriteLine(result2.ToString());

            //CreateCustomerAsync
            var createOrEditCustomerRequest = new CreateOrEditCustomerRequest
            {
                FirstName = "Test",
                LastName = "Test2",
                Email = "test@gamil.com",
                PreferenceIds = new List<Guid> { Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") }
            };

            var result3 = await connection.InvokeAsync<CustomerResponse>("CreateCustomerAsync", createOrEditCustomerRequest);
            Console.WriteLine(result3.ToString());

            //EditCustomerAsync
            createOrEditCustomerRequest = new CreateOrEditCustomerRequest
            {
                FirstName = "Test4444444",
                LastName = "Test2234234",
                Email = "tes234t@gamil.com",
                PreferenceIds = new List<Guid> { Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84") }
            };

            var result4 = await connection.InvokeAsync<bool>("EditCustomerAsync", result3.Id, createOrEditCustomerRequest);
            Console.WriteLine(result4);

            //DeleteCustomerAsync
            var result5 = await connection.InvokeAsync<bool>("DeleteCustomerAsync", result3.Id);
            Console.WriteLine(result5);

            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}

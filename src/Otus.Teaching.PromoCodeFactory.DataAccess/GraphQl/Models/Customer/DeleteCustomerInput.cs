﻿using System;
namespace Otus.Teaching.PromoCodeFactory.DataAccess.GraphQl.Models.Customer
{
    public record DeleteCustomerInput(
        Guid Id
        );
}
